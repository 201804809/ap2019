var button;
var button1;
var buttons = [];
var count = 1;

function setup() {
  background(255);
  let c = createCanvas(windowWidth, windowHeight);
  c.position(0, 0);

  //First Like button
  button = createButton('Like');
}

function draw() {
  //First Like button style
  button.position(windowWidth/2, windowHeight/2);
  button.style("display","inline-block");
  button.style("color","#fff");
  button.style("padding","5px 8px");
  button.style("text-decoration","none");
  button.style("font-size","0.9em");
  button.style("font-weight","normal");
  button.style("border-radius","3px");
  button.style("border","none");
  button.style("text-shadow","0 -1px 0 rgba(0,0,0,.2)");
  button.style("background","#4c69ba");
  button.style("background","-moz-linear-gradient(top, #4c69ba 0%, #3b55a0 100%)");
  button.style("background","-webkit-gradient(linear, left top, left bottom, color-stop(0%, #3b55a0))");
  button.style("background","-webkit-linear-gradient(top, #4c69ba 0%, #3b55a0 100%)");
  button.style("background","-o-linear-gradient(top, #4c69ba 0%, #3b55a0 100%)");
  button.style("background","-ms-linear-gradient(top, #4c69ba 0%, #3b55a0 100%)");
  button.style("background","linear-gradient(to bottom, #4c69ba 0%, #3b55a0 100%)");
  button.style("filter","progid:DXImageTransform.Microsoft.gradient( startColorstr='#4c69ba', endColorstr='#3b55a0', GradientType=0 )");

  //When mouse is pressed duplicate Like buttons
  button.mousePressed(duplicate);

  //MousePressed for all Like buttons
  for (let i = 0; i < buttons.length; i++) {
    buttons[i].mousePressed(duplicate);
  }
}

function duplicate() {
  count = count*2;

  for (let i = 0; i < count; i++) {
    //All new Like buttons
    button1 = createButton('Like');
    button1.position(random(width), random(height));
    button1.style("display","inline-block");
    button1.style("color","#fff");
    button1.style("padding","5px 8px");
    button1.style("text-decoration","none");
    button1.style("font-size","0.9em");
    button1.style("font-weight","normal");
    button1.style("border-radius","3px");
    button1.style("border","none");
    button1.style("text-shadow","0 -1px 0 rgba(0,0,0,.2)");
    button1.style("background","#4c69ba");
    button1.style("background","-moz-linear-gradient(top, #4c69ba 0%, #3b55a0 100%)");
    button1.style("background","-webkit-gradient(linear, left top, left bottom, color-stop(0%, #3b55a0))");
    button1.style("background","-webkit-linear-gradient(top, #4c69ba 0%, #3b55a0 100%)");
    button1.style("background","-o-linear-gradient(top, #4c69ba 0%, #3b55a0 100%)");
    button1.style("background","-ms-linear-gradient(top, #4c69ba 0%, #3b55a0 100%)");
    button1.style("background","linear-gradient(to bottom, #4c69ba 0%, #3b55a0 100%)");
    button1.style("filter","progid:DXImageTransform.Microsoft.gradient( startColorstr='#4c69ba', endColorstr='#3b55a0', GradientType=0 )");

    append(buttons,button1);
  }
}
