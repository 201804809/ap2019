# Capture All #

![Screenshot](onelike.png)
![Screenshot](onemillionlikes.png)

[RUNME](https://cdn.staticaly.com/gl/201804809/ap2019/raw/master/mini_ex4/p5/empty-example/index.html)

I have been experimenting with various data capturing input and interactive devices the last week and I have found that buttons are just one of many objects that can capture date, but after all I chose to make a program with the simple data capturing of buttons to create a focus on the significance of a Like button and its values in the present and the future.

My program consists of a Like button that duplicates every time you press it. It starts out as one button, which I have created with the use of DOM, and continues its expansion for every Like. To make the buttons duplicate, I have created my own function that makes new buttons when the mouse is pressed by using a for loop.

The program is meant to symbolize how far an online post can move only by Likes. A Like entails a lot more than just a number as Gerlitz and Helmond writes in their article The like economy: Social buttons and the date intensive web (page 1359):

> *A Like is always more than a number on the Like counter or more than representational (Thrift, 2008). Its value lies both in the present and in the future, in the plus one it adds to the Like counter and the number of x potential more likes, comments, shares or other responses it might generate within the platform. It is in this sense that we can understand the infrastructure of the Like economy as lively (Marres and Weltevrede, 2012), as changing internally through the numerous ways in which data are multiplied and content is circulating.*

The program does not have a reset function since there is no such function on Facebook. Once you have liked a post, your friends can see it and so forth. The post is scalable in itself and can go viral just by getting likes from enough (or the right) people. Yes, there is actually an Unlike button on Facebook, but once the post is popular enough, what difference does one Like make?
