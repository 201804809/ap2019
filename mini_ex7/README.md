# A Generative Program #

![Screenshot](generativeprogram.png)

[RUNME](https://cdn.staticaly.com/gl/201804809/ap2019/raw/master/mini_ex7/p5/empty-example/index.html)

This week I have been working together with Charlotte to create a rule-based generative program from scratch. I have through this mini exercise formed a bigger understanding for generativity and automatism by experimenting with code trying to programme something that could keep running and at the same time being aesthetically pleasing to look at. Generativity means for a program that it will never stop running.

The finished program consists of an ellipse without no fill and a coloured stroke moving around the screen without a new background being drawn so you can see all the earlier positions. Over time, the screen will be filled with ellipses showing a pattern and the program will continue automatically to create new ones. We implemented three simple rules in our program that all concentrate around the axes of the canvas. Every time the ellipse touches the X axis or Y axis;

1. the direction changes to a specific chosen direction,
2. the colour of the stroke changes to a random colour from an array, we have   created with chosen colours, and
3. the size of the ellipse changes to a random from another array.

The same three rules concern both axes but with different colour arrays for each. The rules of randomness contribute to the creation of a new image, each time the program is refreshed. Our program is a partly autonomous system which is generative on its own. It is only partly autonomous due to the fact that we have chosen to control the randomness of the colour and the size, thus we have set limits on how the program can develop.
