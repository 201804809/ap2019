# Revisiting mini_ex1 #

![Screenshot](nat.png)
![Screenshot](dag.png)

[RUNME](https://glcdn.githack.com/201804809/ap2019/raw/master/mini_ex5/p5/empty-example/index.html)

I have been revisiting my first mini exercise to rework the code and make it better technically. The landscape from the first mini exercise consisted of a 2D picture, which showed a blue sky, a hill and a sun whereas the sun continually moved from the left to the right side of the frame. When creating this program, I found myself frustrated almost throughout the process, first of all because I wasn't able to create what I had intended, why I chose to go back and improve the program with newly acquired coding skills.

My first intentions with the program, was for the sun to go down and then change the colour of the sky and secondly, I was slightly dissatisfied that my program didn't afford interaction. By changing the suns position to the mouse's position, I created this missing interaction and by using and if and else if, I made the colour of the sky dependent on the sun's position. Additionally, the program now has shining stars randomly placed on the sky which seems clearer when the sun's down, considering the background.

The rework of the first mini exercise has showed me how my knowledge of coding has developed since my first coding experience. I chose to change some technically dissatisfaction from the first exercise, why this exercise has been more technical than conceptual for me, as there wasn't any deeper meaning behind in the first place. It was a different experience to work with the program for the second time and forming an overview of what I have learnt has given me new motivation. When I started experimenting with coding, it seemed almost too cumbersome to gasp: A brand new language, where errors are not allowed if the code is to be executed. However, I could definitely see the importance of it. We are moving towards a more digitalized world, wherein coding most likely will be a literacy as reading and writing. In this new digital age, programming and coding will become a part of the foundation of our world.
