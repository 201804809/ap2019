var col = {
  r : 0,
  g : 0,
  b : 25
};

let stars = [];

function setup() {
  createCanvas(800, 600);

  //Stars
  for (i = 1; i < 18; i++) {
    stars.push({
        x: random(800),
        y: random(250),
        color: (50),
        s: random(10)
     });
  }
}

function draw() {
  background(col.r, col.g, col.b);
  rectMode(CENTER);

  //Stars
  colorMode(HSL, 360, 100, 100, 1);
  for (i = 0; i < stars.length; i++) {
    stars[i].color = stars[i].color;
    stars[i].s = stars[i].s % 5 + 0.15;
    stroke(stars[i].color, 70, 80);
    strokeWeight(stars[i].s);
    fill(stars[i].color, 100, 65);
    ellipse(stars[i].x, stars[i].y, 10);
  }
  colorMode(RGB, 100);

  //The sun
  push();
  stroke(255, 90, 0);
  strokeWeight(10);
  fill(255, 80, 0);
  ellipse(mouseX, mouseY, 100, 100, 100);
  pop();

  //The field
  push();
  noStroke();
  fill(color(0, 50, 0));
  rect(400, 450, 800, 300);
  pop();

  //When the sun goes down, it turns dark
  if (mouseY < 350) {
    col.r = 30;
    col.g = 80;
    col.b = 200;
  } else if (mouseY > 350) {
    col.r = 0;
    col.g = 0;
    col.b = 25;
  }
}
