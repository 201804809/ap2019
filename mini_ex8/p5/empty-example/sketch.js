var heart
var with1;
var searching;
var matches = [];
var count = 0;

function preload(){
  with1 = loadJSON("people.json");
  searching = loadJSON("dateMe.json");
  heart = loadAnimation("heart0.png","heart1.png")
}


function setup(){
  createCanvas(windowWidth,windowHeight);

  for (let j = 0; j < 10; j++) {
    var person =  searching.for[j].this; //loop through the people in dateME.json
    for (let i = 0; i < 10; i++) {
      var theMatch;
      var temp_match = with1.thePerson[i].quality; //loop through the people in people.json
      var quality = match(person, temp_match); //check if the the temporary match and person have something in common
      if (quality != null) { //if they do make the match be "the persons name"
        theMatch = with1.thePerson[i].name;
      }
      temp_match = with1.thePerson[i].food;
      var food = match(person, temp_match);
      if (food != null) {
        theMatch = with1.thePerson[i].name;
      }
      var temp_match = with1.thePerson[i].hobby;
      var hobby = match(person, temp_match);
      if (hobby != null) {
        theMatch = with1.thePerson[i].name;
      }
    }
    append(matches, theMatch + " and " + searching.for[j].name); //put the matching people in an array
  }
}


function draw(){
  background(255);

  animation(heart,windowWidth/2,windowHeight/2+60)

  textAlign(CENTER);
  textSize(20)
  fill(255)
  text(matches[count],windowWidth/2,windowHeight/2); //display who matches

  if(frameCount % 40 == 0) {
    count++;
    if (count == 10)
    count = 0
  }
}
