function setup() {
  createCanvas(800, 600);
  background(255);
}

function draw() {
  //face1
  rectMode(CENTER);
  fill(255, 230, 0);
  noStroke();
  ellipse(400, 300, 350, 350);

  //draw on face1
  noStroke();
  fill(0, 25);
  ellipse(mouseX, mouseY, 25, 25);
}

function mousePressed() {
  background(255);

  //face1
  fill(255, 230, 0);
  noStroke();
  ellipse(400, 300, 350, 350);
}
