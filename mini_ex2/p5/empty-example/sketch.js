function setup() {
  createCanvas(800, 600);
  background(255);
  rectMode(CENTER);

  //face1
  fill(255, 230, 0);
  noStroke();
  ellipse(400, 300, 350, 350);
}

function draw() {
  //draw on face1
  noStroke();
  fill(0, 25);
  ellipse(mouseX, mouseY, 25, 25);
}

function mousePressed() {
  background(255);
  rectMode(CENTER);

  //face1
  fill(255, 230, 0);
  noStroke();
  ellipse(400, 300, 350, 350);
}
