var y = 200;
var speed = 2.5;
var diameterX = 100;
var diameterY = 100;

function setup() {
  createCanvas(800, 600);
}

function draw() {
  background(255);

	stroke(0);
  line(0, 400, 800, 400);

  fill(255, 50, 50);
  noStroke();
  ellipse(400, y, diameterX, diameterY);

  //To make the ball bounce
  if (y > 353) {
    speed = -2.5;
  }  else if (y < 200) {
    speed = +2.5;
  }

  if (y > 350) {
    diameterX = diameterX + 3;
    diameterY = diameterY - 3;
  }  else if (y < 340) {
    diameterX = 100;
    diameterY = 100;
  }

  y = y + speed;
}
