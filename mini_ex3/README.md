# My Throbber #

![Screenshot](throbber.png)

[RUNME](https://cdn.staticaly.com/gl/201804809/ap2019/raw/master/mini_ex3/p5/empty-example/index.html)

My program consists of a red ball bouncing up and down which is meant to give the feeling of an ongoing flow. It moves with the same speed continuously. The ball is supposed to present a throbber which is a visual animation used to show that a computer is performing an action invisible for humans.

In this program I have been experimenting with variables and tried to get more familiar with them. By using 'if' and 'else if', I have controlled how the ellipse, which is the ball, moves. I wanted the ball to speed up on the way down and slow down on the way up to make it more realistic, but I didn't succeed finding a fitting syntax or function to make it happen. In other words, the program didn't turn out exactly how I wanted it to. Consequently, it was a more frustrating process for me this time because I wasn't able to create what I thought was very simple. I also had an idea that I wanted to make the ball bounce around the whole canvas on all walls, so the throbber would be a ball bouncing all over the screen. If I could have created this It would also have been fun to implement an aspect of gamification, by creating an interaction with the throbber. You could either control how fast the ball should move or where to.

A throbber helps the human understanding of how the computer works and its presence reassures us that something is going on behind the surface while we are waiting. On that ground it can also become stressing and at times very frustrating if a throbber does not appear, in which case you can't know that your part is done and that you just have to wait for the computer to do its job. Even though the advanced computer processes, that are happening while a throbber is presence, seems very complex, the throbber can yet seem simple for us.

Although the waiting time can be frustrating, I think that we need to appreciate the throbber, since it gives us the information that something is happening while we are waiting. To make the waiting seem less boring and frustrating, the throbber can be gamified. You can be enjoying the time instead of feeling like you are wasting it and since we use technology more and more, we will most likely use a lot more time waiting, so why not make it more fun.

I am disappointed about the fact that I wasn't able to complete my ideas in time, but I have also found it interesting to reflect upon time and temporality in digital culture through throbbers. I will definitely try to work even more on my program to create what was intended, since I think it would be a fun throbber.
