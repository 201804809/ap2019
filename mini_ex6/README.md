# My Game #

My program is not finished yet, but imagine this: The red circle with a face needs to catch the smaller with circles. If not you loose the game.
I will make this happen with dists and a life counter (counting from 10 to 0). If ball reaches the height of the canvas then counter -1. When the dists matches, delete the ball. When the counter = 0, display text ('YOU LOOSE') and then noLoop.

![Screenshot](Game.png)

[RUNME](https://cdn.staticaly.com/gl/201804809/ap2019/raw/master/mini_ex6/p5/empty-example/index.html)

I have been experimenting with object oriented programming by creating a simple mini game with a class-based object oriented approach to design one of my game components. My program consists of a red circle with a face, smaller white circles dropping randomly from the top and a text saying "Get em all". The red circle follows the mouse and the user has to steer it into the white circles to “get em”. If a white circle moves all the way from the top to the bottom of the canvas without the user catching it on the way down, the text "You lose" appears and the game stops.

To make the sprite, red circle with a face, I have used the p5.play library to get the face and then made a function to create the round circle that stretches in the direction of movement proportionally to the speed. The speed is inversely proportional to the mouse distance. To create the sprite I found help and inspiration in the p5.play examples. Making the white circles, I created a class in which I specified the structure of its objects' attributes and behaviour. I made them appear randomly in the top by setting the y-position to 0 and the x-position random.

In this program I have chosen to call the object off the class SuccessInLife, but for the user who does not see the code, I could have named the object Plant or Beer instead, without the appearance of the program had had any visible changes. With the simple expression of the program's components, the user does not get the symbolization that the red circle is oneself, called You in the code, and the white circles is success that one must capture in order not to be a failure. In fact, I could have changed the names to balloon and bubbles without the user would perceive the game differently. It raises the question of why we name our objects and what importance it can have for the program's appearance.
