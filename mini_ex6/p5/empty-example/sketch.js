var you;
var face;
var successes = [];


function setup() {
  createCanvas(1000, 600);

  //setup you
  face = loadImage('assets/face.png');
  you = createSprite(400, 200, 10, 10);

  //edit you and add more to the sprite than the face (the ellipse)
  you.draw = function() {
    fill(255, 51, 51);
    //to make you stretch in the direction of the movement proportionally to the speed
    push();
    rotate(radians(this.getDirection()));
    ellipse(0, 0, 100+this.getSpeed(), 100-this.getSpeed());
    pop();
    //the position increment
    image(face, this.deltaX*2, this.deltaY*2);
  };

  you.maxSpeed = 10;

  //setup successes
  successes[0] = new SuccessInLife();
}


function draw() {
  background(204, 240, 255);

  //text "GET EM ALL"
  strokeWeight(6);
  stroke(230, 252, 255);
  fill(230, 252, 255);
  textSize(120);
  textAlign(CENTER, CENTER);
  text('GET EM ALL', 500, 300);

  //movement of you with speed inversely proportional to the mouse distance
  you.velocity.x = (mouseX-you.position.x)/10;
  you.velocity.y = (mouseY-you.position.y)/10;
  drawSprite(you);

  //successes
  for (let i = 0; i < successes.length; i++) {
   successes[i].move();
   successes[i].show();
   if (successes[i].y > height) {
     successes.push(new SuccessInLife());
     successes.splice(i, 1);
   }
  }
}


class SuccessInLife {
  constructor(size, x, y, velocity) {
    this.size = random(10, 50);
    this.x = random(this.size / 2, width - this.size /2);
    this.y = 0;
    this.velocity = 0.175;
  }
  move() {
    this.velocity +=  0.175;
    this.y = this.y + this.velocity;
  }
  show() {
    strokeWeight(3);
    stroke(250, 250, 255);
    fill(255);
    ellipse(this.x, this.y, this.size, this.size);
  }
}
