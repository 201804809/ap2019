function setup() {
  createCanvas(800, 600);
  frameRate(50)
  rectMode(CENTER);
}

function draw() {
  background('rgb(50%,70%,100%)');

  noStroke();
  fill(color(0, 150, 0));
  ellipse(200, 600, 1300, 400);

  var step = frameCount % 800;
  applyMatrix(1, 0, 0, 1, -150 + step, 80);
  stroke(255, 230, 0);
  strokeWeight(8);
  fill(color(255, 200, 0));
  ellipse(100, 100, 100, 100);
}
